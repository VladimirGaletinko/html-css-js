const files = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function uploadFile(file) {
    return new Promise((resolve, reject) => setTimeout(() => {
        try {
            if (Math.random() > 0.5) {
                throw new Error("Failed to upload file");
            }
            console.log(file);
            resolve();
        } catch (e) {
            reject({error: e, file: file});
        }
    }, parseInt(Math.random() * 300)));
}

startPromise();
async function startPromise() {
    for(let i = 0;i<files.length;i++){
        await uploadFile(files[i]).catch(err=>console.log(`Failed to upload ${err.file}`));
    }
    console.log("Done");
}

//If you like recursion)
// let i = 0;
// startPromise(uploadFile(files[0]));
//
// function startPromise(promise) {
//     promise.then(result => {
//             if (i < files.length) {
//                 startPromise(uploadFile(files[i]));
//             } else{
//                 console.log("Done");
//             }
//         },
//         error => {
//             console.log(`Failed to upload ${error.file}`);
//             if (i < files.length) {
//                 startPromise(uploadFile(files[i]));
//             } else{
//                 console.log("Done");
//             }
//         });
//     i++;
// }
