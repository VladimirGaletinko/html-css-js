function getData() {
    const ARR = new Array(1000);

    for (let i = 0; i < 1000; i++) {
        ARR[i] = { index: `${parseInt(Math.random() * 10)}.${parseInt(Math.random() * 20)}-${parseInt(Math.random() * 100)}` };
    }
    return ARR;
}

const DATA = getData();

DATA.sort((a,b)=>{
    const num1 = a.index.split('-');
    const num2 = b.index.split('-');

    if(parseInt(num1[0]) > parseInt(num2[0])){
        return 1;
    } else if(parseInt(num1[0]) < parseInt(num2[0])){
        return -1;
    } else if(+num1[0].split('.')[1] > +num2[0].split('.')[1]){
        return 1;
    } else if(+num1[0].split('.')[1] < +num2[0].split('.')[1]){
        return -1;
    } else if(+num1[1] > +num2[1]){
        return 1;
    } else if(+num1[1] < +num2[1]){
        return -1;
    } else return 0;
});

