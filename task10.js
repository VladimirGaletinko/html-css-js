const str = "Return please, the length of the longest word in the provided sentence.";

console.log(getTheLongestWord(str));
console.log(getTheLongestWord1(str));
console.log(getTheLongestWord2(str));

//with RegExp
function getTheLongestWord(str) {
    let result = str.split(/[ ,.:;]+/).map((el) => el.length);
    return Math.max(...result);
}
//length++ while no special symbol
function getTheLongestWord1(str) {
    let result = [];
    let length = 0;
    let symbols = " .,!?;:";
    for (let char of str) {
        if (symbols.includes(char)) {
            result.push(length);
            length = 0;
        } else {
            length++;
        }
    }
    return Math.max(...result);
}
//split only with space, if string(element of array) have special symbol then length--, but we have to be sure that
//after comma should be space
function getTheLongestWord2(str) {
    str = str.split(' ');
    let result = [];
    let length;
    let symbols = ".,!?;:";
    for (let value of str) {
        length = value.length;
        for (let char of value) {
            if (symbols.includes(char)) {
                length = value.length - 1;
            }
        }
        result.push(length);
    }
    return Math.max(...result);
}