const arr = [
    'http://example.com',
    'https://example.com',
    'https://example.com/',
    'http://example.csn.khai.edu',
    'example.com',
    'example',
    ':/123@test,com'
];

isMatch(arr);

function isMatch(arr) {
    const regExp = /^(http|https):\/\/([a-zA-Z0-9-_]+\.)*[a-zA-Z]{2,10}?(|\/)$/;

    arr.forEach((item)=>{
        console.log(`${item}  ${regExp.test(item)}`);
    })

}