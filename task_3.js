const generator = sequence(10, 3);

console.log(generator()); // 10
console.log(generator()); // 13

function sequence(_start,_step) {
    let value = _start;
    const step = _step;
    return function () {
        value+=step;
        return value-step;
    }
}
