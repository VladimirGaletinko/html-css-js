const object1 = {a: 1, b: 2, c: 3, r: {e: 2, d: 4}, arr: [1, 2, 3], arrObj: [{z: 4}]};
const object2 = {a: 1, c: 4, d: 5, r: {e: 3, d: 4, z: 3}, arr: [1, 2, 3], arrObj: [{z: 5}]};

console.log(objectCompare(object1, object2));

function objectCompare(obj1, obj2) {
    const map = new Map();
    const added = {};
    const change = {};
    const deleted = {};

    for (let [key, value] of Object.entries(obj2)) {
        map.set(key, value);
    }

    for (let [key, value] of Object.entries(obj1)) {
        if (map.has(key)) {
            if (typeof value === 'object') {
                const temp = objectCompare(value, map.get(key));
                if (!(Object.entries(temp.added).length === 0 && Object.entries(temp.change).length === 0 &&
                    Object.entries(temp.deleted).length === 0)) {

                    for (const key1 in temp) {
                        if (Object.entries(temp[key1]).length === 0) {
                            delete temp[key1];
                        }
                    }

                    if (map.get(key).length === undefined) {
                        map.set(key, {...temp});
                    }
                    change[key] = map.get(key);
                }
            } else if (map.get(key) !== value) {
                change[key] = map.get(key);
            }
        } else {
            deleted[key] = value;
        }
        map.delete(key);
    }

    for (let [key, value] of map.entries()) {
        added[key] = value;
    }

    return {added, change, deleted};
}