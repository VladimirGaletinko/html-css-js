const str = "Given a string, create a function reverseString to return the reverse of it.";

console.log(reverseString(str));
console.log(reverseString1(str));
console.log(reverseString2(str));

function reverseString(str) {
    return str.split('').reverse().join('');
}

function reverseString1(str) {
    let result = [];
    for (let i = str.length - 1; i >= 0; i--) {
        result.push(str[i]);
    }
    return result.join('');
}


function reverseString2(str) {
    let result = "";
    for (let i = 0; i < str.length; i++) {
        result = str[i] + result;
    }
    return result;
}