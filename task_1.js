const users = [
    {
        name: "z",
        surname: "a"
    },
    {
        name: "a",
        surname: "z"
    },
    {
        name: "b",
        surname: "b"
    }
];

byField = byField.bind(users);

function byField(field){
    this.sort((a,b)=>{
        if(a[field] > b[field]){
            return 1;
        }
        if(a[field] < b[field]){
            return -1;
        }
        return 0;
    })
}


users.sort(byField ("name"));
console.log(users);








