const DATA = {
    fieldOne: {
        fieldTwo: {
            fieldThree: () => console.log("test")
        },
        fieldFour: 'test',
        someList: [{ test: 'test'  }]
    }
};

let newObj = deepCopy(DATA);
newObj.fieldOne.fieldFour = "test1";
newObj.fieldOne.someList[0].test = 'test1';

console.log(DATA);
console.log(newObj);


function deepCopy(data) {
    let obj = {};
    if(data.length !==undefined) obj = [];
    for(let [key, value] of Object.entries(data)){
        //console.log(`key: ${key} , value: ${value}`);
        if(typeof value === "object"){
            obj[key] = deepCopy(value);
        }
        else{
            obj[key] = value;
        }
    }
    return obj;
}
