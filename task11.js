const arr = [
    [1, 2, 3, 4],
    [5, 18, 0, 12],
    [3, 5, 12, 5],
    [28, 9, 2, 34],
];

function getTheLargestNumber(arr) {
    const result = [];
    for (let i = 0; i < arr.length; i++) {
        result.push(Math.max(...arr[i]));
    }
    return result;
}

function getTheLargestNumber1(arr){
    const result =[];
    for(let value of arr){
        result.push(value.sort((a,b)=>b-a)[0]);
    }
    return result;
}

function getTheLargestNumber2(arr) {
    const result = [];
    let max;
    for (let i = 0; i < arr.length; i++) {
        max = arr[i][0];
        for (let j = 1; j < arr[i].length; j++) {
            if(max < arr[i][j]){
                max = arr[i][j];
            }
        }
        result.push(max);
    }
    return result;
}

console.log(getTheLargestNumber(arr));
console.log(getTheLargestNumber1(arr));
console.log(getTheLargestNumber2(arr));