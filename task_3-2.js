const gen = sequence(0, 2);
const arr = take(gen, 5);

console.log(arr); // [0, 2, 4, 6, 8]

function sequence(_start,_step) {
    let value = _start;
    const step = _step;
    return function () {
        value+=step;
        return value-step;
    }
}

function take(gen,count) {
    const arr = [];
    for(let i = 0; i< count;i++){
        arr.push(gen());
    }
    return arr;
}