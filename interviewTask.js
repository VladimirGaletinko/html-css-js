function breakChocolateBar(n, m, k) {
    return m > k ? false : k % m !== 0 ? false : n * m >= k;
}

console.log(breakChocolateBar(3, 3, 9));

function uniqueNumbers(arr) {
    const map = new Map();
    let countUnique = 0;

    arr.forEach((item) => {
        if (map.get(item)) {
            const temp = map.get(item);
            map.set(item, temp + 1);
        } else {
            map.set(item, 1);
        }
    });

    map.forEach((value) => {
        if (value === 1) {
            countUnique++;
        }
    });

    console.log(`Count of numbers which appear only one time: ${countUnique}`);
    console.log(`Count of unique numbers: ${map.size}`);
}

uniqueNumbers(generateArray());

function generateArray() {
    const arr = [];
    const length = Math.floor(Math.random() * 1000);
    for (let i = 0; i < length; i++) {
        arr.push(Math.floor(Math.random() * 100));
    }
    return arr;
}

function cutList(A, B) {
    let a = A,
        b = B;
    let res = 0;
    console.log(`a:${a} b:${b}`);
    if (a === b) {
        res = res + (a * b) - ((a - 1) * (b - 1));
    } else if (a < b) {
        res = res+ cutList(a, b - a);
        res++;
    } else if (a > b) {
        res = res + cutList(a-b, b);
        res++;
    }
    return res;
}

console.log(`Count of squares ${cutList(8, 6)}`);
