const data = [
    {
        id: 1,
        parent: null
    },
    {
        id: 2,
        parent: 1
    },
    {
        id: 3,
        parent: 1
    },
    {
        id: 4,
        parent: 1
    },
    {
        id: 5,
        parent: 2
    },
    {
        id: 6,
        parent: 2
    },
    {
        id: 7,
        parent: 3
    },
    {
        id: 8,
        parent: 3
    },
    {
        id: 9,
        parent: 3
    },
    {
        id: 10,
        parent:4
    }
];



formTreeData(data);

function formTreeData(data){
    const tree = {
        "id":data[0].id,
        "parent":data[0].parent,
        "children":[]
    };
    for(let i = 1;i<data.length;i++){
        if(data[i].parent == tree.id){
            tree.children.push(data[i]);
        }
        else{
            findChilds(tree.children,data[i]);
        }
    }

    console.log(tree);

    function findChilds(tree,child){
        for(let i = 0;i<tree.length;i++){
            if(tree[i].id == child.parent){
                if(!tree[i].children){
                    tree[i].children = [child];
                }
                else{
                    tree[i].children.push(child);
                }
            }
            else if(tree[i].children){
                findChilds(tree[i],child);
            }
        }
    }

}

